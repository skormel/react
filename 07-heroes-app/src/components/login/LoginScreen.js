import React from 'react'
import { useHistory } from "react-router-dom";


export const LoginScreen = () => {
  let history = useHistory();


  const handleClick = () => {
    // history.push('/'); // This method add new uri to history stack
    history.replace('/'); // This method replace current uri so no new element it's added to history stack
  }

  return (
    <div className="container mt-5">
      <h1>Login</h1>
      <hr />
      <button
        className="btn btn-primary"
        onClick={handleClick}
      >
        Login
      </button>
    </div>
  )
}
