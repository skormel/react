import React from 'react';
import { shallow } from 'enzyme';
import '@testing-library/jest-dom';

import { MultiCustomHooks } from '../../../components/03-examples/MultiCustomHooks';
import { useFetch } from '../../../hooks/useFetch';
import { useCounter } from '../../../hooks/useCounter';

jest.mock('../../../hooks/useFetch');
jest.mock('../../../hooks/useCounter');

describe('MultiCustomHook tests', () => {

  beforeEach(() => {
    useCounter.mockReturnValue({
      counter: 10,
      increment: () => { }
    });
  })

  test('should render correctly', () => {

    useFetch.mockReturnValue({
      data: null,
      loading: true,
      error: null
    });

    const wrapper = shallow(<MultiCustomHooks />);
    expect(wrapper).toMatchSnapshot();
  });

  test('should render correct information', () => {
    useFetch.mockReturnValue({
      data: [{ author: 'Fake author', quote: 'Hello fake' }],
      loading: false,
      error: null
    });

    const wrapper = shallow(<MultiCustomHooks />);

    // console.log(wrapper.html());  // si wrapper content in html format

    expect(wrapper.find('.alert').exists()).toBe(false);
    expect(wrapper.find('.mb-0').text().trim()).toBe('Hello fake');
    expect(wrapper.find('footer').text().trim()).toBe('Fake author');
  });


});
