import { renderHook, act } from '@testing-library/react-hooks';

import { useForm } from '../../hooks/useForm';

describe('useForm tests', () => {

  const initialForm = {
    name: 'Fake name',
    email: 'fake@email.com'
  }

  test('Should return a default value form', () => {
    const { result } = renderHook(() => useForm(initialForm));

    const [formValues, handleInputChange, reset] = result.current;

    expect(formValues).toEqual(initialForm);
    expect(typeof handleInputChange).toBe('function');
    expect(typeof reset).toBe('function');
  });

  test('Should change form name value', () => {
    const { result } = renderHook(() => useForm(initialForm));

    const [, handleInputChange] = result.current;

    act(() => {
      handleInputChange({
        target: {
          name: 'name',
          value: 'Another fake name'
        }
      });
    });

    const [formValues] = result.current;

    expect(formValues.name).toBe('Another fake name');
    expect(formValues).toEqual({ ...initialForm, name: 'Another fake name' });
  });


  test('Should reset values form', () => {
    const { result } = renderHook(() => useForm(initialForm));

    const [, handleInputChange, reset] = result.current;

    act(() => {
      handleInputChange({
        target: {
          name: 'name',
          value: 'Another fake name'
        }
      });

      reset();
    });

    const [formValues] = result.current;
    expect(formValues).toEqual(initialForm);
  });
});
