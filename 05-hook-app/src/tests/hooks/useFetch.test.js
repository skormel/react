import { renderHook, act } from '@testing-library/react-hooks';

import { useFetch } from '../../hooks/useFetch';

describe('useFetch tests', () => {

  test('Should return a default state', () => {
    const { result } = renderHook(() => useFetch('https://www.breakingbadapi.com/api/quotes/1'));

    const { data, loading, error } = result.current;

    expect(data).toBe(null);
    expect(loading).toBe(true);
    expect(error).toBe(null);
  });

  test('Should return loading and error to false', async () => {
    const { result, waitForNextUpdate, wai } = renderHook(() => useFetch('https://www.breakingbadapi.com/api/quotes/1'));

    await waitForNextUpdate({ timeout: 5000 });

    const { data, loading, error } = result.current;

    expect(data.length).toBe(1);
    expect(loading).toBe(false);
    expect(error).toBe(null);
  });

  test('Should return an error', async () => {
    const { result, waitForNextUpdate, wai } = renderHook(() => useFetch('https://reqres.in/apid/users?page=2'));

    await waitForNextUpdate({ timeout: 5000 });

    const { data, loading, error } = result.current;

    expect(data).toBe(null);
    expect(loading).toBe(false);
    expect(error).toBe('Request error');
  });
});
