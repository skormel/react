import React from 'react';
import { shallow } from 'enzyme';
import '@testing-library/jest-dom';

import { HookApp } from '../HookApp';

describe('Tests over HookApp', () => {
  test('should show hookApp correctly', () => {
    const wrapper = shallow(<HookApp />);
    expect(wrapper).toMatchSnapshot();
  });
})

