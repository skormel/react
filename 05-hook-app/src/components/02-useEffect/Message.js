import React, { useEffect, useState } from 'react'

export const Message = () => {

  const [coords, setCoords] = useState({ x: 0, y: 0 });
  const { x, y } = coords;

  useEffect(() => {

    const moveMouse = (e) => {
      const coords = { x: e.x, y: e.y }
      setCoords(coords);
    }

    window.addEventListener('mousemove', moveMouse);
    return () => {
      window.removeEventListener('mousemove', moveMouse);
    }
  }, [])

  return (
    <>
      <h1>Hello from Message component</h1>
      <h2>Coords: ({x}, {y})</h2>
    </>
  )
}
