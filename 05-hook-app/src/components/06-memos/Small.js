import React, { memo } from 'react'

export const Small = memo(({ value }) => {
  console.log('Called again!');
  return (
    <small>
      {value}
    </small>
  )
});
