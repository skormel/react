import React, { useState } from 'react'
import '../02-useEffect/effects.css';
import { MultiCustomHooks } from '../03-examples/MultiCustomHooks';

export const RealExampleRef = () => {

  const [show, setShow] = useState(false);

  return (
    <div>
      <h1>Real Example Ref</h1>
      <hr />

      <button
        className="btn btn-primary mt-5"
        onClick={() => setShow(!show)}
      >
        Hide/show
      </button>

      {show && <MultiCustomHooks />}
    </div>
  )
}
