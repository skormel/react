import { useEffect, useRef, useState } from "react";

export const useFetch = (url) => {
  const isMounted = useRef(true);
  const [state, setState] = useState({ loading: true, data: null, error: null });

  useEffect(() => {
    return () => {
      return isMounted.current = false;
    }
  }, []);

  useEffect(() => {
    setState({ loading: true, data: null, error: null });

    fetch(url)
      .then(resp => resp.json())
      .then(data => {
        setTimeout(() => {
          if (isMounted.current) {
            setState({
              loading: false,
              error: null,
              data
            });
          }
        }, 2000)
      })
      .catch(() => {
        setState({
          data: null,
          loading: false,
          error: 'Request error'
        })
      })
  }, [url]);

  return state;
}

