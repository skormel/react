describe('Tests from demo.test.js file', () => {
  test('Strings should be equals', () => {
    // AAA

    // #1 Initiation
    const message1 = 'Hello world';

    // #2 Stimulus
    const message2 = `Hello world`;

    // #3 Check
    expect(message1).toBe(message2);
  });
});
