import '@testing-library/jest-dom'; // To get lsp for testing
import { getSaludo } from '../../base/02-template-string';
describe('Test on template-string', () => {
  test('Should return a salute', () => {
    const name = 'Fernando';

    const salute = getSaludo(name);
    expect(salute).toBe('Hola ' + name);
  });

  test('Should return hola Carlos on missing name', () => {
    const salute = getSaludo();
    expect(salute).toBe('Hola Carlos');
  });
});
