import { getUser, getUsuarioActivo, saludar2 } from '../../base/05-funciones';

describe('Tests over 05-funciones', () => {
  test('should return a object', () => {
    const userTest = {
      uid: 'ABC123',
      username: 'El_Papi1502',
    };

    const user = getUser();
    expect(user).toEqual(userTest);
  });

  test('should return an active user', () => {
    const name = 'Paco';
    const userTest = {
      uid: 'ABC567',
      username: name,
    };
    const user = getUsuarioActivo(name);
    expect(user).toEqual(userTest);
  });

  test('should salute a user', () => {
    const name = 'Paco';

    const user = saludar2(name);
    expect(user).toEqual(`Hola, ${name}`);
  });
});
