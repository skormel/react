import { getHeroeByIdAsync } from '../../base/09-promesas';
import heroes from '../../data/heroes';

describe('Test over promises', () => {
  test('should return a async heroe', (done) => {
    const id = 1;
    getHeroeByIdAsync(id).then((heroe) => {
      expect(heroe).toBe(heroes[0]);
      done();
    });
  });

  test('should return error on no existing heroe', (done) => {
    const id = 10;
    getHeroeByIdAsync(id).catch((error) => {
      expect(error).toEqual('No se pudo encontrar el héroe');
      done();
    });
  });
});
