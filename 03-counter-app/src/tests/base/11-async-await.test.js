import { getImagen } from "../../base/11-async-await";

describe('async-await tests', () => {
  test('should return url image', async () => {

    const url = await getImagen();
    expect( typeof url).toBe( 'string');
  });
})
