import React from 'react';
import { shallow } from 'enzyme';
import '@testing-library/jest-dom';

import PrimeraApp from '../PrimeraApp';

describe('Tests over PrimeraApp', () => {
    test('should show hello world message', () => {
      const salute = 'Hello world';
      const wrapper = shallow(<PrimeraApp salute={ salute }/>);

      expect(wrapper).toMatchSnapshot();
  });

  test('should show subtitle send by its prop', () => {
    const salute = 'Hello world';
    const subtitle = 'Its my subtitle';
    const wrapper = shallow(<PrimeraApp
      salute={ salute }
      subtitle={ subtitle }
      />
    );

    const paragraphText = wrapper.find('p').text();
    console.log(paragraphText);

    expect(paragraphText).toBe(subtitle);
  });
})
