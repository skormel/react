import React from 'react';
import { shallow } from 'enzyme'
import CounterApp from '../CounterApp';

describe('tests over CounterApp', () => {

  let wrapper = shallow(<CounterApp/>); // To enable vscode intellilence
  beforeEach( () => {
    wrapper = shallow(<CounterApp/>);
  });

  test('should show counterApp correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });

  test('should show 100 value', () => {
    const wrapper = shallow(<CounterApp value={100}/>);
    const counterValue = wrapper.find('h2').text().trim();

    expect(counterValue).toBe('100');
    console.log(counterValue);
  });

  test('Should increment +1 on +1 button click', () => {
      wrapper.find('button').at(0).simulate('click');
      const counterValue = wrapper.find('h2').text().trim();

      expect(counterValue).toBe('11');
  });

  test('Should decrement -1 on -1 button click', () => {
    wrapper.find('button').at(2).simulate('click');
    const counterValue = wrapper.find('h2').text().trim();

    expect(counterValue).toBe('9');
  });

  test('sholuld reset value on reset button click', () => {
    const wrapper = shallow(<CounterApp value={105}/>);

    wrapper.find('button').at(0).simulate('click');
    wrapper.find('button').at(0).simulate('click');

    let counterValue = wrapper.find('h2').text().trim();
    expect(counterValue).toBe('107');

    wrapper.find('button').at(1).simulate('click');

    counterValue = wrapper.find('h2').text().trim();
    expect(counterValue).toBe('105');
  });


})
