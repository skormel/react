import React from 'react';
import PropTypes from 'prop-types';

// Functional Component
const PrimeraApp = ({ salute, subtitle }) => {
  return (
    <>
      <h1>{salute}</h1>
      <p>{subtitle}</p>
    </>
  );
};

PrimeraApp.propTypes = {
  salute: PropTypes.string.isRequired,
};

PrimeraApp.defaultProps = {
  subtitle: "I'm a subtitle",
};

export default PrimeraApp;
